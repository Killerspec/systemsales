package com.ait.systemSales.model;

import javax.xml.bind.annotation.XmlRootElement;
import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

@Entity
@XmlRootElement
@Table(name="systemsales")
public class SystemSales {
	
	@Id 
	
	private String partNumber;
	private String partName;
	private int stockNumbers;
	private String manufacturerType;
	private double benchMark;
	private String manufacturer;
	private double price;
	private String picture;
	
	public String getManufacturer() {
		return manufacturer;
	}
	public void setManufacturer(String manufacturer) {
		this.manufacturer = manufacturer;
	}
	public String getPictures() {
		return picture;
	}
	public void setPictures(String picture) {
		this.picture = picture;
	}
	public void setPartNumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartNumber() {
		return partNumber;
	}
	public void setPartumber(String partNumber) {
		this.partNumber = partNumber;
	}
	public String getPartName() {
		return partName;
	}
	public void setPartName(String partName) {
		this.partName = partName;
	}
	public int getStockNumbers() {
		return stockNumbers;
	}
	public void setStockNumbers(int stockNumbers) {
		this.stockNumbers = stockNumbers;
	}
	public String getManufacturerType() {
		return manufacturerType;
	}
	public void setManufacturerType(String manufacturerType) {
		this.manufacturerType = manufacturerType;
	}
	public double getBenchMark() {
		return benchMark;
	}
	public void setBenchMark(double benchMark) {
		this.benchMark = benchMark;
	}
	public String getManuacturer() {
		return manufacturer;
	}
	public void setManuacturer(String manuacturer) {
		this.manufacturer = manuacturer;
	}
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	
	
}
