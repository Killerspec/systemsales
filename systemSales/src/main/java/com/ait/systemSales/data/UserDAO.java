package com.ait.systemSales.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.systemSales.model.Users;

@Stateless
@LocalBean
public class UserDAO {
	
	@PersistenceContext
	private EntityManager em;
			
	public List<Users> getAllUsers(){
		Query query = em.createQuery("Select u from Users u");
		return query.getResultList();
	}
	
		
	public Users getUser( String username ) {
		return em.find(Users.class, username);
	}
	
	@SuppressWarnings("unchecked")
	public List<Users> findAllUsernames(){
		String sql = "select * from users;";
		javax.persistence.Query query = em.createNativeQuery(sql);
		return query.getResultList();
		}
	
	@SuppressWarnings("unchecked")
	public List<Users> retrieveUserByString(String columnName, String queryString){
		String sql = "select * from users where "+columnName+" like '%" + queryString +"%';";
		javax.persistence.Query query = em.createNativeQuery(sql);
		return query.getResultList();
		}
	
	public void create(Users users) {
		em.persist(users);
	}
	
	public void update(Users users) {
		em.merge(users);
	}
	
	public void delete(String username) {
		System.out.println(username);
		em.remove(getUser(username));
	}
}
