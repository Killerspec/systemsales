package com.ait.systemSales.data;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;
import javax.persistence.Query;

import com.ait.systemSales.model.SystemSales;

@Stateless
@LocalBean
public class SystemDAO {
	
	@PersistenceContext
	private EntityManager em;
	
	public List<String> getAllParts(){
		Query query = em.createQuery("Select u from SystemSales u");
		return query.getResultList();
	}
	
	public List <SystemSales> getSomeParts(){
		Query query = em.createNativeQuery("Select partName, manufacturer from SystemSales ;");
		return query.getResultList();
	}
	
	public SystemSales getParts( String partNumber ) {
		return em.find(SystemSales.class, partNumber);
	}
	
	@SuppressWarnings("unchecked")
	public List<SystemSales> retrievePartsByString(String columnName, String queryString){
		String sql = "select * from systemsales where "+columnName+" like '%" + queryString +"%' ";
		javax.persistence.Query query = em.createNativeQuery(sql);
		return query.getResultList();
		}
	
	public void create(SystemSales systemSales) {
		em.persist(systemSales);
	}
	
	public void update(SystemSales systemSales) {
		em.merge(systemSales);
	}
	
	public void delete(String partNumber) {
		em.remove(getParts(partNumber));
	}
}
