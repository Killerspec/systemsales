package com.ait.passwordSecurity;

import java.security.MessageDigest;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;;

public class PasswordSecurity {

	
	
	public static String get_SHA_512_SecurePassword(final String passwordToHash, final String salt) {
		String generatedPassword = null;
		try {
			final MessageDigest md = MessageDigest.getInstance("SHA-512");
			md.update(salt.getBytes());
			final byte[] bytes = md.digest(passwordToHash.getBytes());
			final StringBuilder sb = new StringBuilder();
			for (int i = 0; i < bytes.length; i++) {
				sb.append(Integer.toString((bytes[i] & 0xff) + 0x100, 16).substring(1));
			}
			generatedPassword = sb.toString();
		} catch (NoSuchAlgorithmException e) {
			e.printStackTrace();
		}
		return generatedPassword;
	}

	public static String getSalt() throws NoSuchAlgorithmException {
		final SecureRandom sr = SecureRandom.getInstance("SHA1PRNG");
		final byte[] salt = new byte[16];
		sr.nextBytes(salt);
		return salt.toString();
	}
	
	public static String encryptPasword(final String password) throws NoSuchAlgorithmException {
		final String salt = getSalt();
		String encyptedPassword = get_SHA_512_SecurePassword(password, salt);
		encyptedPassword = salt+":"+encyptedPassword;		
		return encyptedPassword;
	}
	
	public String verifyOldPassword(final String oldPassword, final String encryptedPassword) {
		final String [] splitPassword = encryptedPassword.split(":");
		final String salt = splitPassword[0];		
		String encryptedOldPassword = get_SHA_512_SecurePassword(oldPassword, salt);
		encryptedOldPassword = salt+":"+encryptedOldPassword;
		return encryptedOldPassword;
	}
}
