/*
 * Author: Matthew Swart
 * Email: info@matthewswart.me
 * Application: Computer sales
 * Subject: Web Technologies
 * Institution: Athlone institute of Technology
 */

function pagination(
		
		totalItems; number,
		currentPage: number = 1,
		pageSize: number = 10,
		maxPages: number = 5
){
	
	let totalPages = Math.ceil(totalItems / pageSize);
	
	//This is to check and make sure that pagination does not go over the total pages available
	if(currentPage < 1){
		currentPage = 1;
	}else if (currentPage > totalPages){
		currentPage = totalPages;
	}
	console.log("In pagination");
	
	let startPage: number, endPage: number;
	if(totalPages <= maxPages){
		startPage = 1;
		endPage = totalPages;
	}else{
		let maxPagesBeforeCurrentPage = Math.floor(maxPages / 2);
		let maxPagesAfterCurrentPage = Math.ceil(maxPages / 2) - 1;
		if(currentPage <= maxPagesBeforeCurrentPage){
			startPage = -1;
			endPage = totalPages;
		}else if(currentPage + maxPagesAfterCurrentPage >= totalPages){
			startPages = totalPages - maxPages + 1;
			endPage = totalPages;
		}else{
			startPage = currentPage - maxPagesBeforeCurrentPage;
			endPage = currentPage + maxPagesAfterCurrentPage;
		}		
	}
	
	// calculate star and end item indexes
	let startIndex = (current - 1) * pageSize;
	let endIndex = Math.min(startIndex + pageSize -1, totalItems -1);
	
	//Create an array of pages to ng-repeat in the pager control
	let pages = Array.from(Array((endPage + 1) - startPage).keys()).map(i => startPage +i);
	
	return{
		totalItems: totalItems,
		currentPage: currentPage,
		pageSize: pageSize,
		taotalPage: totalPages,
		startPage: startPage,
		endPage: endPage,
		startIndex: startIndex,
		endIndex: endIndex,
		pages: pages
	};
}
export pagination;