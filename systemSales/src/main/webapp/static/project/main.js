/**
 * Author: Matthew Swart
 * Email: info@matthewswart.me
 * Application: Computer sales
 * Subject: Web Technologies
 * Institution: Athlone institute of Technology
 */

var baseURL = "http://localhost:8080/systemSales/rest";

var findAllUsers = function() {
	console.log("inside findAllUsers")
	$.ajax({
		type: 'GET',
		url: baseURL+"/users",
		dataType: "json",
		success: renderUserList
	});
}

var findAllParts = function() {
	console.log("inside findAllParts")
	$.ajax({
		type: 'GET',
		url: baseURL+"/parts",
		dataType: "json",
		success: function(data, status, jqXHR){
			console.log(data)
			$('#partsTableRightSide').DataTable( {
	            //pageLength: 6,
	            //lengthMenu: [6, 12, 24],
	            "data": data,
	            colums:[
	            	{"data": "0"},
	            	{"data": "1"}            	
	            ],
	            //"paging": true
	        });
		}
	});
}



var renderUserList = function(data){
	console.log("Render user list");
	/*list = data == null ? [] : (data instanceof Array ? data : [ data ]);*/
	console.log(data);	
	var count = 1;
	var htmlCode = ""
	$.each(data, function(index, users){
		console.log(users.username);
		$('#userTable').append('<tr><th>'+count+'</th><th>'+users.username+'</th><th>'+users.userType+'</th><th><a href="#" id="'+users.userame+'"class="fa fa-info-circle"></a></th><th>'+
				'<a href="#" id="'+users.username+'"class="fa fa-trash"></a></th></tr>');
		count++;
	});
	$('#userTableLeftSide').DataTable({
		"pageLength": 6,
		"lengthMenu": [[6,12,24],[6,12,24]]
	});
}

let renderPartsList = function(data){
	console.log(data);
	let table = $('#partsTableRightSide');
	var count = 1;
	table.DataTable( {
            pageLength: 6,
            lengthMenu: [6, 12, 24],
            data: data,
            colums:[
            	{"data": "partNumber"},
            	{"data": "partName"}            	
            ],
            "paging": true
        });
}

/*var renderPartsList = function(data){
	console.log("Render parts list");
	list = data == null ? [] : (data instanceof Array ? data : [ data ]);
	var count = 1;
	var htmlCode = "";
	$.each(data, function(index, parts){
		console.log(parts[0]);
		$('#partsTable').append('<tr><th>'+count+'</th><th>'+parts[1]+'</th><th>'+parts[5]+'</th><th>'+parts[4]+'</th>'+
				'<th><a href="#" id="'+parts[0]+'"class="fa fa-info-circle"></a></th><th>'+
				'<a href="#" id="'+parts[0]+'"class="fa fa-trash"></a></th></tr>');
		count++;
	});
	$('#partsTableRightSide').DataTable({
		"pageLength": 6,
		"lengthMenu": [[6,12,24],[6,12,24]]
	});
}*/

var deleteUser = function(username){
	console.log("username in delete "+username);
	console.log(baseURL+'/users/'+username);
	$.ajax({
		type: 'DELETE',
		contentType : "application/json",
		url : baseURL+'/users/'+username,
		dataType : "json",
		success : function(data, textStatus, jqXHR) {
			alert("User deleted successfully.");
		},
		error : function(jqXHR, textStatus, errorThrown) {
			alert("Error adding user - " + textStatus)
		} 
	});
}

/*var formToJSON = function(){
	return JSON.stringify({
		"username": $('#username').val(),
		"email": $('#userEmail').val(),
		"password": $('#userPassword').val(),
		"userType": $('userType').val(),
		"firstName": $('#userFirstname').val(),
		"lastName": $('#userLastname').val(),
		"contactNumber": $('#UserContactNumber').val()
	})
}*/

$(document).ready(function(){
	
	$(document).on("click", ".fa-info-circle", function(){
		userInfo(this.username);
	});
	$(document).on("click", ".fa fa-trash", function(){
		console.log("Delete has been pressed "+this.id);
		deleteUser(this.id);
		location.reload();
		findAllUsers();
		});
	
	

	findAllUsers();
	findAllParts();
	
})