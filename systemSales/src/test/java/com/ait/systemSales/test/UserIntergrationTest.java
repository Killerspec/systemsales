package com.ait.systemSales.test;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertTrue;

import java.security.NoSuchAlgorithmException;
import java.util.List;

import javax.ejb.EJB;
import javax.ws.rs.Path;
import javax.ws.rs.core.Response;

import org.jboss.arquillian.container.test.api.Deployment;
import org.jboss.arquillian.junit.Arquillian;
import org.jboss.shrinkwrap.api.Archive;
import org.jboss.shrinkwrap.api.ShrinkWrap;
import org.jboss.shrinkwrap.api.asset.EmptyAsset;
import org.jboss.shrinkwrap.api.spec.JavaArchive;
import org.junit.After;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import com.ait.systemSales.model.*;
import com.ait.systemSales.data.*;
import com.ait.systemSales.rest.*;

@RunWith(Arquillian.class)
public class UserIntergrationTest {

	@Deployment
	public static Archive<?> createTestArchive() {
		return ShrinkWrap.create(JavaArchive.class, "Test.jar")
				//.addClasses(UserDAO.class, User.class, RestApplication.class, UserWS.class, UtilsDAO.class)
				.addPackages(true, "com.ait")
				.addAsManifestResource("META-INF/persistence.xml", "persistence.xml")
				.addAsManifestResource(EmptyAsset.INSTANCE, "beans.xml");
	} 
	
	@EJB
	private UserWS userWS;

	@EJB
	private UserDAO userDAO;
	
	@EJB
	private UtilsDAO utilsDAO;
	
	List<Users> userList;
	Users user;
	
	final String BASE_REST_URL = "http://localhost:8080/systemSales/rest/users";
	
	@Before
	public void setUp() {
		utilsDAO.deleteUserTable();
		user = new Users();
		user.setUsername("Matt");
		user.setEmail("a@a.com");
		user.setPassword("1234");
		user.setUserType("admin");
		user.setFirstName("John");
		user.setLastName("Swart");
		user.setContactNumber("00000");
		userDAO.create(user);
		userList = userDAO.getAllUsers();
	}
	
	@Test 
	public void testCreateUser() throws NoSuchAlgorithmException {
		
		final Users bob = new Users();
		bob.setUsername("Bob");
		bob.setEmail("b@b.com");
		bob.setPassword("12345");
		bob.setUserType("user");
		bob.setFirstName("Bob");
		bob.setLastName("Kane");
		bob.setContactNumber("00000");
		
		Response response = userWS.saveUser(bob);
		assertEquals(201, response.getStatus());
		Users returned = (Users) response.getEntity();
		
		assertEquals("Bob", returned.getUsername());
		assertEquals("Bob", returned.getFirstName());
		assertEquals("user", returned.getUserType());
	}
	
	@Test
	public void testRemoveUser() {
		Response response = userWS.deleteUser("Matt");
		assertEquals(204, response.getStatus());
		userList = userDAO.getAllUsers();
		assertEquals("testRemoveUser", 0, userList.size());
	}
	
	@Test
	public void testUserByUserName()  {
		Response response = userWS.findUsersById("Matt");
		Users returned = (Users) response.getEntity();
		assertEquals("Matt", returned.getUsername());
		
		Response notFoundResponse = userWS.findUsersById("Bob");
		assertEquals(404, notFoundResponse.getStatus());
		Users notReturned = (Users) notFoundResponse.getEntity();
		assertEquals(null, notReturned.getUsername());
	}
	
	@Test
	public void testSearchByColumNameAndQueryType() {
		Response response = userWS.retrieveUserByString("userType", "admin");
		assertEquals(200, response.getStatus());
		
		Response errorResponse = userWS.retrieveUserByString("userType", "sales");
		assertEquals(404, errorResponse.getStatus());
	}
		
	@After
	public void tearDown() {
		utilsDAO.deleteUserTable();
	}
	
}
