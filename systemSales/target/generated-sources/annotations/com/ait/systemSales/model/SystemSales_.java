package com.ait.systemSales.model;

import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value = "org.hibernate.jpamodelgen.JPAMetaModelEntityProcessor")
@StaticMetamodel(SystemSales.class)
public abstract class SystemSales_ {

	public static volatile SingularAttribute<SystemSales, Integer> stockNumbers;
	public static volatile SingularAttribute<SystemSales, String> manufacturerType;
	public static volatile SingularAttribute<SystemSales, Double> price;
	public static volatile SingularAttribute<SystemSales, Double> benchMark;
	public static volatile SingularAttribute<SystemSales, String> partNumber;
	public static volatile SingularAttribute<SystemSales, String> partName;
	public static volatile SingularAttribute<SystemSales, String> picture;
	public static volatile SingularAttribute<SystemSales, String> manufacturer;

}

