CREATE DATABASE  IF NOT EXISTS cpusales;
USE cpusales;


DROP TABLE IF EXISTS systemsales;

CREATE TABLE systemsales (
  PartNumber varchar(45) not null,
  PartName varchar(100),
  StockNumbers int(11) DEFAULT NULL,
  ManufacturerType varchar(100),
  BenchMark double DEFAULT NULL,
  Manufacturer varchar(100),
  Price double DEFAULT NULL,
  Picture varchar(100),
  PRIMARY KEY (PartNumber)
) ;
INSERT INTO `systemsales` VALUES ('31009','3 2200G',1,'Ryzen',81.41,'AMD',89.99,'32200G.jpg'),
('31013','3 1200',14,'Ryzen',74.9,'AMD',89.99,'31200.jpg'),('31034','3 1300X',4,'Ryzen',55.53,'AMD',134.99,'31300X.jpg'),
('51005','5 1006X',12,'Ryzen',83.72,'AMD',157.94,'51006X.jpg'),('51007','5 1600',3,'Ryzen',82.4,'AMD',148.9,'51600.jpg'),
('51008','5 2600',10,'Ryzen',82.1,'AMD',164.99,'52600.jpg'),('51014','5 2600X',4,'Ryzen',71.85,'AMD',199.99,'52600X.jpg'),
('51018','5 1500X',13,'Ryzen',69.51,'AMD',144.99,'51500X.jpg'),('51023','5 2400G',5,'Ryzen',66.31,'AMD',139.99,'52400G.jpg'),
('51030','5 1400',2,'Ryzen',57.67,'AMD',144.99,'51400.jpg'),('70133','7 1800X',14,'Ryzen',55.98,'AMD',275.21,'71800X.jpg'),
('71010','7 1700',17,'Ryzen',80.85,'AMD',169.99,'71700.jpg'),('71012','7 1700X',13,'Ryzen',76.67,'AMD',191.38,'71700X.jpg'),
('71028','7 2700',9,'Ryzen',60.03,'AMD',249.99,'72700.jpg'),('71032','7 2700X',2,'Ryzen',56.62,'AMD',299.99,'72700X.jpg'),
('A101019','A10-9700',9,'A10',68,'AMD',79.99,'A10-9700.jpg'),('A101020','A10-9700X',13,'A10',67.84,'AMD',69.99,'A10-9700X.jpg'),
('A101045','A10-7800APU',12,'A10',45.88,'AMD',103.39,'A10-7800APU.jpg'),('A121042','A12-9800',12,'A12',49.11,'AMD',109.99,'A12-9800.jpg'),
('A121049','1920X',2,'Ryzen',44.59,'AMD',433.91,'1920X.jpg'),('A61017','A6-7400K',13,'A6',69.59,'AMD',39.95,'A6-7400K.jpg'),
('A61038','A6-9500',12,'A6',50.89,'AMD',56.4,'A6-9500.jpg'),('A81011','A8-9600',13,'A8',76.71,'AMD',65.11,'A8-9600.jpg'),
('A81025','A8-7600APU',9,'A8',63.39,'AMD',79.99,'A8-7600APU.jpg'),('A81036','A8-7650K',14,'A8',54.26,'AMD',89.96,'A8-7650K.jpg'),
('E51016','E5-2660 v2',6,'XEON',70.23,'INTEL',189,'E5-2660v2.jpg'),('E51027','E5-1620 v3',5,'XEON',61.05,'INTEL',159.99,'E5-1620v3.jpg'),
('FX1006','FX-8320E',7,'FX',82.74,'AMD',91.21,'FX-8320E.jpg'),('FX1024','FX-8370',1,'FX',65.62,'AMD',136.99,'FX-8370.jpg'),
('G31029','G3900',8,'Celeron',59.93,'INTEL',52.62,'G3900.jpg'),('G41031','G4900',10,'Celeron',57.45,'INTEL',57.49,'G4900.jpg'),
('G41044','G49560',12,'Pentium',48.2,'INTEL',100.69,'G49560.jpg'),('GE1003','200GE',16,'Athlon',90.09,'AMD',55,'200GE.jpg'),
('i31021','i3-8100',7,'CORE I',67.74,'INTEL',118.99,'i3-8100.jpg'),('i31046','i3-8350K',7,'CORE I',45.81,'INTEL',201.05,'i3-8350K.jpg'),
('i51015','i5-9400F',6,'CORE I',71.03,'INTEL',175.99,'i5-9400F.jpg'),('i51026','i5-8400',11,'CORE I',62.85,'INTEL',184.99,'i5-8400.jpg'),
('i51037','i5-8500',6,'CORE I',53.4,'INTEL',219,'i5-8500.jpg'),('i51039','i5-8400T',5,'CORE I',50.73,'INTEL',189,'i5-8400T.jpg'),
('i51040','i5-9600K',9,'CORE I',50.66,'INTEL',264.99,'i5-9600K.jpg'),('i51043','i5-4590',17,'CORE I',48.34,'INTEL',149,'i5-4590.jpg'),
('i51047','i3-8300',17,'CORE I',45.06,'INTEL',189.07,'i3-8300.jpg'),('i71050','A12-9800E',13,'A12',44.37,'AMD',114.49,'A12-9800E.jpg'),
('TH1041','1900X',3,'Ryzen',50.13,'AMD',315.94,'1900X.jpg'),('TH1048','i5-4460',17,'CORE I',44.67,'INTEL',149,'i5-4460.jpg'),
('X41001','X4 845',16,'Athlon',101.49,'AMD',53.39,'X4845.jpg'),('X41002','X4 860K',8,'Athlon',99.51,'AMD',54.5,'X4860K.jpg'),
('X41004','X4 950',15,'Athlon',84.43,'AMD',62.29,'X4950.jpg'),('X41022','X4 870K',17,'Athlon',66.5,'AMD',79.99,'X4870K.jpg'),
('X41035','X4 880K',18,'Athlon',55.35,'AMD',99.99,'X4880K.jpg');


DROP TABLE IF EXISTS users;

CREATE TABLE users (
  username varchar(25) not null,
  email varchar(255),
  password varchar(50),
  userType varchar(45),
  firstName varchar(100),
  lastName varchar(100),
  contactNumber varchar(45),
  PRIMARY KEY (username)
);


INSERT INTO users VALUES ('Matt','matt@matt.com','12345','admin','Matthew','Swart','0868288923'),('Theo','matt@matt.com','12345','user','Matthew','Swart','0868288923');


