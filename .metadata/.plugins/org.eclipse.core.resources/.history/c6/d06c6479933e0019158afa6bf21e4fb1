package com.ait.systemSales.rest;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.systemSales.data.UserDAO;
import com.ait.systemSales.model.SystemSales;
import com.ait.systemSales.model.Users;

@Path("/users")
@Stateless
@LocalBean
public class UserWS {
	
	@EJB
	private UserDAO userDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllUsers() {
		List<Users> users = userDao.getAllUsers();
		return Response.status(200).entity(users).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{username}")
	public Response findUsersById(@PathParam("username") String username) {
		Users users = userDao.getUser(username);
		if(users == null) {
			return Response.status(200).entity(new Users()).build();
		}
		return Response.status(200).entity(users).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response saveUser(Users users) {
		userDao.create(users);
		return Response.status(201).entity(users).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/retrieve/{queryString}-{queryString2}")
	public Response retrieveUserByString(@PathParam("queryString") String columnName,@PathParam("queryString2") String query) {
		System.out.println("Column name: "+columnName+" Query: "+query);
		List<Users> userInfo = userDao.retrieveUserByString(columnName, query);
		if(userInfo.isEmpty()) {
			return Response.status(404).build();
		}
		return Response.status(200).entity(userInfo).build();
	}
	
	@PUT
	@Path("/{userame}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updateUser(Users users) {
		userDao.update(users);
		return Response.status(200).entity(users).build();
	}
	
	@DELETE
	@Path("/{username}")
	public Response deleteUser(@PathParam("username") String username) {
		userDao.delete(username);
		return Response.status(204).build();
	}
}
