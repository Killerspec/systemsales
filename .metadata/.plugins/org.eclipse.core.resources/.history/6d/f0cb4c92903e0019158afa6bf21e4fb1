package com.ait.systemSales.rest;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

import com.ait.systemSales.data.SystemDAO;
import com.ait.systemSales.model.SystemSales;

@Path("/parts")
@Stateless
@LocalBean
public class SystemWS {
	
	@EJB
	private SystemDAO systemDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllProducts() {
		List<SystemSales> systemSales = systemDao.getAllParts();
		return Response.status(200).entity(systemSales).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{partNumber}")
	public Response findPartsByPartNumber(@PathParam("partNumber") String partNumber) {
		SystemSales systemSales = systemDao.getParts(partNumber);
		return Response.status(200).entity(systemSales).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/retrieve/{queryString}")
	public Response retrievePartsByString(@PathParam("queryString")String columnName, String query) {
		List<SystemSales> systemSales = systemDao.retrievePartsByString(columnName, query);
		return Response.status(200).entity(systemSales).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response savePart(SystemSales systemSales) {
		systemDao.create(systemSales);
		return Response.status(201).entity(systemSales).build();
	}
	
	@PUT
	@Path("/{partNumber}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updatePart(SystemSales systemSales) {
		systemDao.update(systemSales);
		return Response.status(200).entity(systemSales).build();
	}
	
	@DELETE
	@Path("/{partNumber}")
	public Response deleteParts(@PathParam("partNumber") String partNumber) {
		systemDao.delete(partNumber);
		return Response.status(204).build();
	}
}
