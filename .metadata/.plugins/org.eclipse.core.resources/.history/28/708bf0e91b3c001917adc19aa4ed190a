package com.ait.systemSales.rest;

import java.util.List;

import javax.ejb.LocalBean;
import javax.ejb.Stateless;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.DELETE;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PUT;
import javax.ws.rs.Path;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.MediaType;
import javax.ws.rs.core.Response;

@Path("/parts")
@Stateless
@LocalBean
public class systemWS {
	
	@EJB
	private SystemDAO systemDao;
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	public Response findAllProducts() {
		System.out.println("Get all system parts");
		List<SystemSales> systemSales = systemDao.getAllParts();
		return Response.status(200).entity(systemSales).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/{id}")
	public Response findPartsById(@PathParam("id") int id) {
		SystemSales systemSales = systemDao.getParts(id);
		return Response.status(200).entity(systemSales).build();
	}
	
	@GET
	@Produces({ MediaType.APPLICATION_JSON })
	@Path("/retrieve/{queryString}")
	public Response retrievePartsByString(@PathParam("queryString") String query) {
		List<SystemSales> systemSales = systemDao.retrievePartsByString(query);
		return Response.status(200).entity(systemSales).build();
	}
	
	@POST
	@Produces({ MediaType.APPLICATION_JSON })
	public Response savePart(SystemSales systemSales) {
		systemDao.save(systemSales);
		return Response.status(201).entity(systemSales).build();
	}
	
	@PUT
	@Path("/{id}")
	@Consumes("application/json")
	@Produces({ MediaType.APPLICATION_JSON })
	public Response updatePart(SystemSales systemSales) {
		System.out.println(systemSales);
		systemDao.update(systemSales);
		return Response.status(200).entity(systemSales).build();
	}
	
	@DELETE
	@Path("/{id}")
	public Response deleteParts(@PathParam("id") int id) {
		systemDao.delete(id);
		return Response.status(204).build();
	}
}
